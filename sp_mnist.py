import pylab as pl
import numpy as np
import gzip
import _pickle as cPickle

class pcn:

    def __init__(self, inputs, targets):
        # Set up network size
        if np.ndim(inputs) > 1:
            self.nIn = np.shape(inputs)[1]
        else:
            self.nIn = 1

        if np.ndim(targets) > 1:
            self.nOut = np.shape(targets)[1]
        else:
            self.nOut = 1

        self.nData = np.shape(inputs)[0]

        # Initialise network
        #self.weights = [[-0.05], [-0.02], [0.02]]
        #print("okkk")
        #print(self.weights)
        self.weights = np.random.rand(self.nIn+1,self.nOut)*0.1-0.05

    def pcntrain(self, inputs, targets, eta, nIterations):
        inputs = np.concatenate((-np.ones((self.nData, 1)), inputs ), axis=1)
        #print("inputs:")
        #print(inputs)
        change = range(self.nData)

        for n in range(nIterations):
            self.activations = self.pcnfwd(inputs);

            #print(self.activations)
            #print(targets)

            for i in range(self.nData):
                if((self.activations[i][0] != targets[i][0])):
                    #print("problem"+str(i))
                    for j in range(np.shape(self.weights)[0]):
                        #print("in"+str(inputs[i][j]))
                        self.weights[j][0]+=(eta*(targets[i][0]-self.activations[i][0])*inputs[i][j])
                    break

            print("Iteration: ", n)
            print(self.weights)

            activations = self.pcnfwd(inputs)
            print("Final outputs are:")
            print(activations)

    def pcnfwd(self, inputs):

        # Compute activations
        activations = np.dot(inputs, self.weights)
        #print("in pcd")
        #print(activations)
        # Threshold the activations
        return np.where(activations > 0, 1, 0)

    def confmat(self, inputs, targets):
        """Confusion matrix"""

        # Add the inputs that match the bias node
        inputs = np.concatenate((inputs, -np.ones((self.nData, 1))), axis=1)
        outputs = np.dot(inputs, self.weights)

        nClasses = np.shape(targets)[1]

        if nClasses == 1:
            nClasses = 2
            outputs = np.where(outputs > 0, 1, 0)
        else:
            # 1-of-N encoding
            outputs = np.argmax(outputs, 1)
            targets = np.argmax(targets, 1)

        cm = np.zeros((nClasses, nClasses))
        for i in range(nClasses):
            for j in range(nClasses):
                cm[i, j] = np.sum(np.where(outputs == i, 1, 0) * np.where(targets == j, 1, 0))

        print(cm)
        print(np.trace(cm) / np.sum(cm))

# Read the dataset in (code from sheet)
f = gzip.open('mnist.pkl.gz','rb')
tset, vset, teset = cPickle.load(f, encoding='latin1')
f.close()

nread = 200
# Just use the first few images
train_in = tset[0][:nread,:]

# This is a little bit of work -- 1 of N encoding
# Make sure you understand how it does it
train_tgt = np.zeros((nread,10))
for i in range(nread):
    train_tgt[i,tset[1][i]] = 1

test_in = teset[0][:nread,:]
test_tgt = np.zeros((nread,10))
for i in range(nread):
    test_tgt[i,teset[1][i]] = 1

# Train a Perceptron on training set
p = pcn(train_in, train_tgt)
p.pcntrain(train_in, train_tgt,0.25,100)

# This isn't really good practice since it's on the training data,
# but it does show that it is learning.
p.confmat(train_in,train_tgt)

# Now test it
p.confmat(test_in,test_tgt)