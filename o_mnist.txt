 ****************** hidden layers  **************** 1
0.1
100
Percentage Correct:  26.5
0.1
200
Percentage Correct:  26.0
0.1
300
Percentage Correct:  25.5
0.2
100
Percentage Correct:  22.5
0.2
200
Percentage Correct:  27.0
0.2
300
Percentage Correct:  24.0
0.30000000000000004
100
Percentage Correct:  26.5
0.30000000000000004
200
Percentage Correct:  25.5
0.30000000000000004
300
Percentage Correct:  23.0
0.4
100
Percentage Correct:  25.5
0.4
200
Percentage Correct:  29.5
0.4
300
Percentage Correct:  27.0
0.5
100
Percentage Correct:  25.0
0.5
200
Percentage Correct:  31.0
0.5
300
Percentage Correct:  26.5
0.6
100
Percentage Correct:  25.5
0.6
200
Percentage Correct:  22.5
0.6
300
Percentage Correct:  27.500000000000004
0.7
100
Percentage Correct:  22.0
0.7
200
Percentage Correct:  23.5
0.7
300
Percentage Correct:  24.5
0.7999999999999999
100
Percentage Correct:  25.0
0.7999999999999999
200
Percentage Correct:  23.5
0.7999999999999999
300
Percentage Correct:  23.5
0.8999999999999999
100
Percentage Correct:  25.5
0.8999999999999999
200
Percentage Correct:  24.0
0.8999999999999999
300
Percentage Correct:  34.0
 ****************** hidden layers  **************** 2
0.1
100
Percentage Correct:  48.5
0.1
200
Percentage Correct:  36.0
0.1
300
Percentage Correct:  39.5
0.2
100
Percentage Correct:  46.0
0.2
200
Percentage Correct:  45.0
0.2
300
Percentage Correct:  39.0
0.30000000000000004
100
Percentage Correct:  41.0
0.30000000000000004
200
Percentage Correct:  33.0
0.30000000000000004
300
Percentage Correct:  37.5
0.4
100
Percentage Correct:  34.5
0.4
200
Percentage Correct:  32.0
0.4
300
Percentage Correct:  35.5
0.5
100
Percentage Correct:  38.5
0.5
200
Percentage Correct:  39.5
0.5
300
Percentage Correct:  40.0
0.6
100
Percentage Correct:  43.5
0.6
200
Percentage Correct:  40.0
0.6
300
Percentage Correct:  41.0
0.7
100
Percentage Correct:  36.5
0.7
200
Percentage Correct:  39.5
0.7
300
Percentage Correct:  44.5
0.7999999999999999
100
Percentage Correct:  42.5
0.7999999999999999
200
Percentage Correct:  40.5
0.7999999999999999
300
Percentage Correct:  39.5
0.8999999999999999
100
Percentage Correct:  37.5
0.8999999999999999
200
Percentage Correct:  41.5
0.8999999999999999
300
Percentage Correct:  38.0
 ****************** hidden layers  **************** 5
0.1
100
Percentage Correct:  63.5
0.1
200
Percentage Correct:  64.0
0.1
300
Percentage Correct:  58.5
0.2
100
Percentage Correct:  67.0
0.2
200
Percentage Correct:  60.5
0.2
300
Percentage Correct:  63.0
0.30000000000000004
100
Percentage Correct:  63.5
0.30000000000000004
200
Percentage Correct:  60.5
0.30000000000000004
300
Percentage Correct:  62.5
0.4
100
Percentage Correct:  65.0
0.4
200
Percentage Correct:  61.0
0.4
300
Percentage Correct:  63.0
0.5
100
Percentage Correct:  67.5
0.5
200
Percentage Correct:  66.0
0.5
300
Percentage Correct:  57.99999999999999
0.6
100
Percentage Correct:  61.0
0.6
200
Percentage Correct:  61.5
0.6
300
Percentage Correct:  56.99999999999999
0.7
100
Percentage Correct:  61.5
0.7
200
Percentage Correct:  64.5
0.7
300
Percentage Correct:  65.0
0.7999999999999999
100
Percentage Correct:  59.0
0.7999999999999999
200
Percentage Correct:  57.99999999999999
0.7999999999999999
300
Percentage Correct:  61.0
0.8999999999999999
100
Percentage Correct:  59.5
0.8999999999999999
200
Percentage Correct:  60.0
0.8999999999999999
300
Percentage Correct:  65.5
 ****************** hidden layers  **************** 10
0.1
100
Percentage Correct:  71.0
0.1
200
Percentage Correct:  70.0
0.1
300
Percentage Correct:  70.0
0.2
100
Percentage Correct:  65.5
0.2
200
Percentage Correct:  69.0
0.2
300
Percentage Correct:  69.0
0.30000000000000004
100
Percentage Correct:  65.5
0.30000000000000004
200
Percentage Correct:  66.0
0.30000000000000004
300
Percentage Correct:  72.5
0.4
100
Percentage Correct:  68.5
0.4
200
Percentage Correct:  65.5
0.4
300
Percentage Correct:  70.5
0.5
100
Percentage Correct:  70.5
0.5
200
Percentage Correct:  68.0
0.5
300
Percentage Correct:  69.5
0.6
100
Percentage Correct:  69.0
0.6
200
Percentage Correct:  64.0
0.6
300
Percentage Correct:  65.5
0.7
100
Percentage Correct:  64.5
0.7
200
Percentage Correct:  70.0
0.7
300
Percentage Correct:  66.0
0.7999999999999999
100
Percentage Correct:  65.5
0.7999999999999999
200
Percentage Correct:  73.5
0.7999999999999999
300
Percentage Correct:  67.5
0.8999999999999999
100
Percentage Correct:  62.0
0.8999999999999999
200
Percentage Correct:  66.0
0.8999999999999999
300
Percentage Correct:  68.5
 ****************** hidden layers  **************** 20
0.1
100
Percentage Correct:  72.5
0.1
200
Percentage Correct:  71.5
0.1
300
Percentage Correct:  71.0
0.2
100
Percentage Correct:  73.5
0.2
200
Percentage Correct:  71.5
0.2
300
Percentage Correct:  70.0
0.30000000000000004
100
Percentage Correct:  64.5
0.30000000000000004
200
Percentage Correct:  68.5
0.30000000000000004
300
Percentage Correct:  69.5
0.4
100
Percentage Correct:  71.0
0.4
200
Percentage Correct:  68.5
0.4
300
Percentage Correct:  72.5
0.5
100
Percentage Correct:  69.5
0.5
200
Percentage Correct:  69.0
0.5
300
Percentage Correct:  71.5
0.6
100
Percentage Correct:  70.0
0.6
200
Percentage Correct:  69.5
0.6
300
Percentage Correct:  67.0
0.7
100
Percentage Correct:  69.0
0.7
200
Percentage Correct:  69.5
0.7
300
Percentage Correct:  70.0
0.7999999999999999
100
Percentage Correct:  71.0
0.7999999999999999
200
Percentage Correct:  70.5
0.7999999999999999
300
Percentage Correct:  73.5
0.8999999999999999
100
Percentage Correct:  72.0
0.8999999999999999
200
Percentage Correct:  69.5
0.8999999999999999
300
Percentage Correct:  70.5
