import numpy as np


class pcn:
    def __init__(self, inputs, targets):
        """ Constructor """
        # Set up network size
        if np.ndim(inputs) > 1:
            self.nIn = np.shape(inputs)[1]
        else:
            self.nIn = 1

        if np.ndim(targets) > 1:
            self.nOut = np.shape(targets)[1]
        else:
            self.nOut = 1

        self.nData = np.shape(inputs)[0]

        # Initialise network
        self.weights = np.random.rand(self.nIn + 1, self.nOut) * 0.1 - 0.05

    def pcntrain(self, inputs, targets, eta, nIterations):
        """ Train the thing """
        # Add the inputs that match the bias node
        inputs = np.concatenate((inputs, -np.ones((self.nData, 1))), axis=1)

        # Training
        change = range(self.nData)

        for n in range(nIterations):
            self.activations = self.pcnfwd(inputs);
            self.weights -= eta * np.dot(np.transpose(inputs), self.activations - targets)
            #print("Iteration: ", n)
            #print(self.weights)

            activations = self.pcnfwd(inputs)
            #print("Final outputs are:")
            #print(activations)

    # return self.weights

    def pcnfwd(self, inputs):
        """ Run the network forward """

        # Compute activations
        activations = np.dot(inputs, self.weights)

        # Threshold the activations
        return np.where(activations > 0, 1, 0)

    def confmat(self, inputs, targets):
        """Confusion matrix"""

        # Add the inputs that match the bias node
        inputs = np.concatenate((inputs, -np.ones((self.nData, 1))), axis=1)
        outputs = np.dot(inputs, self.weights)

        nClasses = np.shape(targets)[1]

        if nClasses == 1:
            nClasses = 2
            outputs = np.where(outputs > 0, 1, 0)
        else:
            # 1-of-N encoding
            outputs = np.argmax(outputs, 1)
            targets = np.argmax(targets, 1)

        cm = np.zeros((nClasses, nClasses))
        for i in range(nClasses):
            for j in range(nClasses):
                cm[i, j] = np.sum(np.where(outputs == i, 1, 0) * np.where(targets == j, 1, 0))

        #print(cm)
        print(np.trace(cm) / np.sum(cm) *100)


pima = np.loadtxt('pima-indians-diabetes.txt',delimiter=',')

# Plot the first and second values for the two classes
indices0 = np.where(pima[:,8]==0)
indices1 = np.where(pima[:,8]==1)


# Perceptron training on the original dataset
print("**************** Output on original data *********************8")
p =pcn(pima[:,:8],pima[:,8:9])


prea = 50
while(prea<=150):
    eapr = 0.125
    while(eapr<=4):
        print(prea)
        print(eapr)
        p.pcntrain(pima[:, :8], pima[:, 8:9], eapr, prea)
        p.confmat(pima[:, :8], pima[:, 8:9])
        #fn=p.activations
        #print(fn)
        eapr = eapr + eapr
    prea = prea + 50




# Various preprocessing steps
pima[np.where(pima[:,0]>8),0] = 8
pima[np.where(pima[:,7]<=30),7] = 1
pima[np.where((pima[:,7]>30) & (pima[:,7]<=40)),7] = 2
pima[np.where((pima[:,7]>40) & (pima[:,7]<=50)),7] = 3
pima[np.where((pima[:,7]>50) & (pima[:,7]<=60)),7] = 4
pima[np.where(pima[:,7]>60),7] = 5

pima[:,:8] = pima[:,:8]-pima[:,:8].mean(axis=0)
pima[:,:8] = pima[:,:8]/pima[:,:8].var(axis=0)

#print pima.mean(axis=0)
#print pima.var(axis=0)
#print pima.max(axis=0)
#print pima.min(axis=0)

trainin = pima[::2,:8]
testin = pima[1::2,:8]
traintgt = pima[::2,8:9]
testtgt = pima[1::2,8:9]

# Perceptron training on the preprocessed dataset
print("*************************8 Output after preprocessing of data *************************8")
p1 = pcn(trainin,traintgt)

prea = 50
while(prea<=150):
    eapr = 0.125
    while(eapr<=4):
        print(prea)
        print(eapr)
        p1.pcntrain(trainin, traintgt, eapr, prea)
        p1.confmat(testin, testtgt)
        #fn=p.activations
        #print(fn)
        eapr = eapr + eapr
    prea = prea + 50




